class JobSeekersController < ApplicationController
  def show
    @job_seeker = JobSeeker.find(params[:id])
  end
  def new
    @job_seeker = JobSeeker.new
  end
  def create
    @job_seeker = JobSeeker.new(job_seeker_params)
    if @job_seeker.save
      log_in @job_seeker,'JobSeeker'
      flash[:success] = "Welcome to the JALAPENO!"
      redirect_to @job_seeker
    else
      render 'new'
    end
  end
  def edit
    @job_seeker = JobSeeker.find(params[:id])
  end

  def update
    @job_seeker = JobSeeker.find(params[:id])
    if @job_seeker.update_attributes(job_seeker_params)
      flash[:success] = "Profile updated"
      redirect_to @job_seeker
    else
      render 'edit'
    end
  end

  private
  def job_seeker_params
    params.require(:job_seeker).permit(:name, :email, :full_name,:phone,:password,
                                 :password_confirmation)
  end
end
