50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  Recruiter.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end
Admin.create!(name:  "flame", email: "xma21@ncsu.edu", password:"rubyiscool", password_confirmation: "rubyiscool")